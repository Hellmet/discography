1. Clone repository; git clone git@bitbucket.org:Hellmet/discography.git
2. Install 'ChromeDriver/WebDriver' on the local machine for JS tests
3. run 'bundle install'
4. run 'rake db:seed'
5. run 'rails s'
6. run 'rspec' to run tests
