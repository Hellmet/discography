require 'rails_helper'

feature "Ajax search", js: true do
  background do
    load "#{Rails.root}/db/seeds.rb"
    @artist = Artist.find_by(name: "Extremoduro")
  end

  def pause
    $stderr.write 'Press enter to continue'
    $stdin.gets
  end

  scenario "LP index shows lps and can search" do
    visit lps_path
    expect(page).to have_text("La Ley Innata")
    fill_in "keyword", with: "Extremoduro"
    click_on "Search"
    expect(page).to have_text("Canciones Prohibidas")
    expect(page).to_not have_text("...And Justice For All")
    fill_in "keyword", with: ""
    click_on "Search"
    expect(page).to have_text("...And Justice For All")
  end

end

feature "Can manage LPs" do
  background do
    load "#{Rails.root}/db/seeds.rb"
    @artist = Artist.find_by(name: "Extremoduro")
  end

  scenario "Clicking an LP from index takes you to artist page for that LP" do
    visit lps_path
    click_on "La Ley Innata"
    expect(page).to have_text("Extremoduro are a Spanish hard rock band from Plasencia, Extremadura.")
  end

  scenario "Can edit an LP" do
    visit artist_path(@artist)
    first(".edit-lp-link").click
    expect(page).to have_text("Edit Canciones Prohibidas")
    fill_in "Description", with: "Edited"
    click_on "Save"
    expect(page).to have_text("LP updated succesfully")
    expect(page).to have_text("Edited")
  end

  scenario "Can add a new LP" do
    visit new_lp_path
    expect(page).to have_text("Add an LP")
    fill_in "Name", with: "Test LP"
    fill_in "Description", with: "Test description"
    select "Queen", from: "Artist"
    click_on "Save"
    expect(page).to have_text("LP created succesfully")
  end

  scenario "Can delete an LP" do
    visit artist_path(@artist)
    first(".delete-lp-link").click
    expect(page).to have_text("LP deleted succesfully")
    expect(page).to_not have_text("Canciones Prohibidas")
  end

  scenario "Errors show when fields arent filled in" do
    visit new_lp_path
    click_on "Save"
    expect(page).to have_text("Artist must exist")
    expect(page).to have_text("Description can't be blank")
    expect(page).to have_text("Name can't be blank")
  end
end
