require 'rails_helper'

feature "Manage artists" do
  background do
    load "#{Rails.root}/db/seeds.rb"
    @artist = Artist.find_by(name: "Extremoduro")
  end

  scenario "Root/index shows artists and descriptions" do
    visit root_path
    expect(page).to have_text("Extremoduro")
    expect(page).to have_text("Extremoduro are a Spanish hard rock band from Plasencia, Extremadura.")
  end

  scenario "Clicking artist goes to show page and lists LPs" do
    visit root_path
    click_on "Extremoduro"
    expect(page).to have_text("Extremoduro")
    expect(page).to have_text("La Ley Innata")
    expect(page).to_not have_text("Revolutionary")
  end

  scenario "Can edit an artist. Edit form header shows title correctly." do
    visit artist_path(@artist)
    within ".artist-header" do
      click_on "Edit"
    end
    expect(page).to_not have_text("Add an Artist")
    expect(page).to have_text("Edit #{@artist.name}")
    fill_in "Description", with: "Edited"
    click_on "Save"
    expect(page).to have_text("Artist updated succesfully")
    expect(page).to have_text("Edited")
    expect(page).to_not have_text("Extremoduro are a Spanish hard rock band from Plasencia, Extremadura.")
  end

  scenario "Can create an artist" do
    visit new_artist_path
    expect(page).to have_text("Add an Artist")
    fill_in "Name", with: "Test band"
    fill_in "Description", with: "Test description"
    click_on "Save"
    expect(page).to have_text("Artist created succesfully")
  end

  scenario "Can delete an artist" do
    visit artist_path(@artist)
    within ".artist-header" do
      click_on "Delete"
    end
    expect(page).to have_text("Artist deleted succesfully")
  end

  scenario "Errors show if fields left blank" do
    visit new_artist_path
    click_on "Save"
    expect(page).to have_text("Description can't be blank")
    expect(page).to have_text("Name can't be blank")
  end

end
