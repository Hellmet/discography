class ArtistsController < ApplicationController

  def new
    @artist = Artist.new
  end

  def create
    @artist = Artist.new(artist_params)
    if @artist.save
      flash[:success] = ["Artist created succesfully"]
      redirect_to artist_path(@artist)
    else
      flash.now[:danger] = @artist.errors.full_messages
      render :new
    end
  end

  def show
    @artist = Artist.find(params[:id])
    @lps = @artist.lps.order(:name)
  end

  def index
    @artists = Artist.all.order(:name)
  end

  def edit
    @artist = Artist.find(params[:id])
  end

  def update
    @artist = Artist.find(params[:id])
    if @artist.update(artist_params)
      flash[:success] = ["Artist updated succesfully"]
      redirect_to artist_path(@artist)
    else
      flash.now[:danger] = @artist.errors.full_messages
      render :edit
    end
  end

  def destroy
    @artist = Artist.find(params[:id]).destroy
    flash[:success] = ["Artist deleted succesfully"]
    redirect_to root_path
  end

  private

  def artist_params
    params.require(:artist).permit(:name, :description)
  end

end
