class LpsController < ApplicationController

  def new
    @lp = Lp.new
  end

  def create
    @lp = Lp.new(lp_params)
    if @lp.save
      flash[:success] = ["LP created succesfully"]
      redirect_to artist_path(params[:lp][:artist_id])
    else
      params[:artist] = @lp.artist.id if params[:artist]
      flash.now[:danger] = @lp.errors.full_messages
      render :new
    end
  end

  def show
    @lp = Lp.find(params[:id])
  end

  def index
    if params[:keyword]
      @lps = Lp.joins(:artist).where("artists.name LIKE ?", "%" + params[:keyword] + "%").order(:name)
    else
      @lps = Lp.includes(:artist).all.order(:name)
    end
    respond_to do |format|
      format.js
      format.html
    end
  end

  def edit
    @lp = Lp.find(params[:id])
  end

  def update
    @lp = Lp.find(params[:id])
    if @lp.update(lp_params)
      flash[:success] = ["LP updated succesfully"]
      redirect_to artist_path(params[:lp][:artist_id])
    else
      params[:artist] = @lp.artist.id if params[:artist]
      flash.now[:danger] = @lp.errors.full_messages
      render :edit
    end
  end

  def destroy
    @lp = Lp.find(params[:id]).destroy
    flash[:success] = ["LP deleted succesfully"]
    redirect_to artist_path(@lp.artist)
  end

  private

  def lp_params
    params.require(:lp).permit(:name, :description, :artist_id)
  end

end
