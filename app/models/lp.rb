class Lp < ApplicationRecord
  belongs_to :artist

  validates :description, :name, presence: true

end
