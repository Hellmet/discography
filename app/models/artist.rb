class Artist < ApplicationRecord
  has_many :lps, dependent: :destroy

  validates :name, uniqueness: true
  validates :description, :name, presence: true
end
