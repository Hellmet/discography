module LpsHelper
  def add_lp_form_header
    if action_name == "edit"
      return "Edit #{@lp.name}"
    end
    "Add an LP"
  end
end
