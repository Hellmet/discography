module ArtistsHelper
  def add_form_header
    if action_name == "edit"
      return "Edit #{@artist.name}"
    end
    "Add an Artist"
  end
end
